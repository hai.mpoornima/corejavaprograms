//Sir, tracing the program is done in separate pdf for your reference.
public class ReverseNumber {

	public static void main(String[] args) {
		int n = 123, rev = 0;  
		while(n != 0)   // when 123, 12, 1 condition loop is true, when n=0 comes out of loop
		{  
		int remainder = n % 10;  // gives remainder,stores only values in that iteration as local variable with in while loop.
		rev = rev * 10 + remainder;  //Each time reversed number is stored 3,32,321 at each iteration
		n = n/10;  // stores quotient 12, 1, 0 at each iteration
		}  
		System.out.println("The reverse of the given number is: " + rev);  
		}  

	}


